﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaturdayTask
{
    class Art
    {
        public string Drawing(string utensil)
        {
            return "Material needed is " + utensil;
        }

        public int Drawing(int priceOfPaper,int priceOfColour)
        {
            int totalPrice = priceOfPaper + priceOfColour;
            return totalPrice;
        }

        public string Drawing(string drawingName, int drawingId)
        {
            return drawingName +" "+ drawingId;
        }
    }
}
