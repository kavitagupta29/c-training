﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SaturdayTask
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Pen p1=new Pen();
            p1.SetPenInfo("Blue","Camlin",1000.0,20,true);
            Console.WriteLine(p1.GetPenInfo());
            Console.WriteLine("Discount on price is:" +p1.GetPrice());

            //Default Constructor call
            Pen p2=new Pen();
            Console.WriteLine(p2.GetPenInfo());
           

            //Paramerterized Constructor call
            Pen p3=new Pen("Black","stack",30000,15,false);
            Console.WriteLine(p3.GetPenInfo());
          

            //Copy Constructor
            Pen p4=new Pen(p3);
            Console.WriteLine(p4.GetPenInfo());
          
            p4.GetPrice();
            p4.GetDiscount(30000);
            Console.WriteLine(p4.GetPrice());

            PensList.AddPens();
            Pen[] pn = PensList.GetPens();
            foreach (Pen p in pn)
            {
                Console.WriteLine(p.GetPenInfo());
                p.GetDiscount(20);
                Console.WriteLine("After discount:");
                Console.WriteLine(p.GetPenInfo());

            }
            Console.ReadLine();

        }

        static void Main1(string[] args)
        {
            Art a=new Art();
            Console.WriteLine(a.Drawing("Drawing paper"));
            Console.WriteLine(a.Drawing(200,100));
            Console.WriteLine(a.Drawing("Nature Painting",1));
            Console.ReadLine();
        }

        static void Main2(string[] args)
        {
            
            PensList.AddPens();
            Pen[] pn = PensList.GetPens();
            foreach (Pen p in pn)
            {
                Console.WriteLine(p.GetPenInfo());
            }
            Console.ReadLine();
        }
    }
}
