﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SaturdayTask
{
    class Pen
    {
        private string colour;
        private string brand;
        private double price;
        private int size;
        private Boolean isFountain;

        private static int count;
        private static string name;

        private static Pen[] penList;


        // static constructor
        static Pen()
        {
            Console.WriteLine("Static constructor is called....");
            count = 0;
            name = "Pen";
        }

        //Default Constructor
        public Pen()
        {
            Console.WriteLine(" Default Constructor called...");
            this.colour = "Red";
            this.brand = "Cello";
            this.price = 4000;
            this.size = 20;
            this.isFountain = false;

        }

        //Parameterized Constructor
        public Pen(string colour, string brand, double price, int size, Boolean isFountain)
        {
            Console.WriteLine("Paramerterized Constructor called...");
            this.colour = colour;
            this.brand = brand;
            this.price = price;
            this.size = size;
            this.isFountain = isFountain;
        }

        //Copy Constructor
        public Pen(Pen p)
        {
            Console.WriteLine("Copy Constructor called...");
            this.colour = p.colour;
            this.brand = p.brand;
            this.price = p.price;
            this.size = p.size;
            this.isFountain = p.isFountain;
        }

        public void SetPenInfo(string colour, string brand, double price, int size, Boolean isFountain)
        {
            this.colour = colour;
            this.brand = brand;
            this.price = price;
            this.size = size;
            this.isFountain = isFountain;
        }

        public string GetPenInfo()
        {
            return "Pen Info: " + " " + this.colour + " " + this.brand + " " + this.price + " " + this.size + " " + this.isFountain;
        }

        public double GetPrice()
        {
            return this.price;
        }

        public void GetDiscount(double disc)
        {
          //int  discount = Convert.ToInt32(this.price);
          //int calculateDiscount = Convert.ToInt32(100 / discount);
            double discount = (price) - ((price*disc)/100);
            this.price = discount;
        }

        public static int GetCount()
        {
            return count;
        }

        public static string GetPenName()
        {
            return name;
        }

        public static void SetPenName(string nm)
        {
            name = nm;
        }

        static void PensList()
        {
            penList = new Pen[4];
        }
        public static void AddPens()
        {
            penList[0] = new Pen("Blue", "Cello", 2000, 20, false);
            penList[1] = new Pen("Red", "globus", 5000, 15, true);
            penList[2] = new Pen("Black", "camline", 1000, 10, false);
            penList[3] = new Pen("Green", "apsara", 2000, 20, true);

        }
       
        public static Pen[] GetPens()
        {
            return penList;
        }

       
    }
}
