﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaturdayTask
{
  static  class PensList
  {
      private static Pen[] penList;

      static PensList()
      {
          penList=new Pen[4];
      }

      public static void AddPens()
      {
          penList[0]=new Pen("Blue","Cello",2000,20,false);
          penList[1] = new Pen("Red", "globus", 5000, 15, true);
          penList[2] = new Pen("Black", "camline", 1000, 10, false);
          penList[3] = new Pen("Green", "apsara", 2000, 20, true);
            
      }

      public static Pen[] GetPens()
      {
          return penList;
      }


    }
}
