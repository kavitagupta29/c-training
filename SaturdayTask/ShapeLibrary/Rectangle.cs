﻿namespace ShapeLibrary
{
 public   class Rectangle:Shape
    {
        private double length;
        private double breadth;

        public Rectangle(string name, double length, double breadth) : base(name)
        {
            this.length = length;
            this.breadth = breadth;
        }

        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() + " " + this.length + " " + this.breadth;
        }

        public override double CalArea()
        {
            return this.length*this.breadth;
        }
    }
}
