﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary
{
 public   class Tringle:Shape
    {
        private double bse;
        private double height;

        public Tringle(string name, double bse, double height) : base(name)
        {
            this.bse = bse;
            this.height = height;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() + " " + this.bse+ " "+ this.height;
        }

        public override double CalArea()
        {
            return (this.bse * this.height)/2;
        }

    }
}
