﻿namespace ShapeLibrary
{
   public class Circle:Shape
    {
        private double radius;

        public Circle(string name, double radius) : base(name)
        {
            this.radius = radius;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() + " " + this.radius;
        }

        public override double CalArea()
        {
            return 3.14 * this.radius*this.radius;
        }
    }
}
