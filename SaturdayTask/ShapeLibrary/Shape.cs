﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary
{
    public abstract class Shape
    {
        private string name;

        //Parameterized Constructor
        public Shape(string name)
        {
            this.name = name;
        }

        public void SetShapeInfo(string name)
        {
            this.name = name;
        }

        public virtual string GetShapeInfo()
        {
            return "Shape info: "+ this.name;
        }

        //public virtual double CalArea()
        //{
        //    return 0;
        //}

        public abstract double CalArea();
    }
}
