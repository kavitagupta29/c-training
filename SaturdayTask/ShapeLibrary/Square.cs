﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary
{
   public class Square:Shape
    {
        private double side;

        public Square(string name,double side):base(name)
        {
            this.side = side;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() + " " + this.side;
        }

        public override double CalArea()
        {
            return this.side * this.side;
        }
    }
}
