﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShapeLibrary;
using System.Threading;

namespace MultiThreadingWinForm
{
    public partial class Form1 : Form
    {
        private List<Shape> sList;
        public Form1()
        {
            InitializeComponent();

            sList = new List<Shape>();


        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread t1 = new Thread(AddItem);

            Thread backgroundThread = new Thread(
        new ThreadStart(() =>
        {
            for (int n = 0; n < 100; n++)
            {
                Thread.Sleep(50);
                progressBar1.Value = n;
            }


            progressBar1.Value = 0;
        }
    ));
            t1.Start();
            backgroundThread.Start();

        }



        public void AddItem()
        {
            for (int i = 0; i <= 20; i++)
            {
                sList.Add(new Circle("Circle", 45));
            }

            listBox1.Items.Clear();
            foreach (Shape s in sList)
            {

                Console.WriteLine(listBox1.Items.Add(s.GetShapeInfo() + "Area is = " + s.CalArea()));
           
                Thread.Sleep(300);
                
            }

            MessageBox.Show("Thread completed!");
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            /* for (int i = 1; i <= 100; i++)
             {
                 // Wait 50 milliseconds.  
                 Thread.Sleep(50);
                 // Report progress.  
                 backgroundWorker1.ReportProgress(i);
             }*/
        }
    }
}
