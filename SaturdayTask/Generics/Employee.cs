﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    class Employee
    {
        private int id;
        private string name;
        protected double salary;

        static int count;
        static string companyName;

        // static constructor
        static Employee()
        {
            Console.WriteLine("Static Constructor is called....");
            count = 100;
            companyName = "Tricentis";
        }
        
        //Default Constructor
        public Employee()
        {
            Console.WriteLine(" Default Constructor called...");
            this.id = 100;
            this.name = "Pranita";
            this.salary = 20000;
            count++;
            
        }

        //parameterized construtor
        public Employee(int id, string name, double salary)
        {
            Console.WriteLine("Paramerterized Constructor called...");
            this.id = id;
            this.name = name;
            this.salary = salary;
            count++;

        }

        public Employee(string name, int id, double salary)
        {
            Console.WriteLine("Paramerterized Constructor called...");
            this.name = name;
            this.id = id;
            this.salary = salary;
            count++;
        }

        //copy Constructor

        public Employee(Employee e)
        {
            Console.WriteLine("Copy Constructor called...");
            this.id = e.id;
            this.name = e.name;
            this.salary = e.salary;
            count++;
        }

        

        public void SetEmpInfo(int id, string nm, double salary)
        {
            this.id = id;
            name = nm;
            this.salary = salary;
        }

        public virtual string GetEmpInfo()
        {
            return "Emp Info:" +" " +this.id +" " + this.name +" " + this.salary +" " +companyName ;
        }

        public virtual double CalSal()
        {
            return this.salary;
        }

        public void InSal(double inc)
        {
            this.salary += inc;
        }

        public static int GetCount()
        {
            return count;
        }

        public static string GetCompany()
        {
            return companyName;
        }

        public static void SetCompany(string cn)
        {
            companyName = cn;
        }

    }
}
