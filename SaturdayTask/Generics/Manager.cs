﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    class Manager:Employee
    {
        private double bonus;
        private string dept;

        public Manager(int id, string name, double salary, double bonus, string dept) : base(id, name, salary)
        {
            this.bonus = bonus;
            this.dept = dept;
        }

        //public new string GetEmpInfo()  -----for Function hiding(new)
        public override string GetEmpInfo()
        {
            return base.GetEmpInfo() + " " + this.bonus + " " + this.dept + "---Mgr";
        }
        //public new double CalSal()  -----for Function hiding(new)
        public override double CalSal()
        {
            return this.salary + this.bonus;
        }
    }
}
