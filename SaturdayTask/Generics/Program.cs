﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeManagement;

namespace Generics
{
    class Program
    {
        static void Main1(string[] args)
        {
            MyArray<string> names=new MyArray<string>(5);
            names.setItem(0,"Kavita");
            names.setItem(1, "Manisha");
            names.setItem(2, "Harshad");
            names.setItem(3, "Shweta");
            names.setItem(4, "Pranita");

            if (names.GetItem(2) == "Harshad")
            {
                names.setItem(2,names.GetItem(1).ToUpper());
            }
            names.DisplayAll();

            MyArray<int> num=new MyArray<int>(3);
            num.setItem(0,1);
            num.setItem(1, 2);
            num.setItem(2, 3);

            if (num.GetItem(1) == 2)
            {
                num.setItem(2,num.GetItem(2));
            }
            num.DisplayAll();

            MyArray<Engineer> eng=new MyArray<Engineer>(3);
            eng.setItem(0,new Engineer(1,"Kavita",2000,4,30000));
            eng.setItem(1, new Engineer(2, "Manisha", 6000, 9, 90000));
            eng.setItem(2, new Engineer(3, "Pranita", 1000, 2, 60000));

            foreach (Engineer e in eng.myArr)
            {
                Console.WriteLine(e.GetEmpInfo());
            }

            List<MyArray<int>> list=new List<MyArray<int>>();
            list.Add(new MyArray<int>(3));
        }

        public static void Swap<T>(ref T val1, ref T val2)
        {
            T tmp = val1;
            val1 = val2;
            val2 = tmp;
        }
        static void Main(string[] args)
        {
            int x = 5,y=6;
            Swap(ref x,ref y);
            char c1 = 'a', c2 = 'b';
            Swap(ref c1,ref c2);
            Console.WriteLine("swapping of x={0},y={1},c1={2},c2={3} ", x,y,c1,c2);
           

        }
    }
}
