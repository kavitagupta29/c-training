﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class MyArray<T>
    {
        public T[] myArr;

        public MyArray(int size)
        {
            myArr=new T[size];
        }

        public T GetItem(int index)
        {
            return myArr[index];
        }

        public void setItem(int index, T val)
        {
            myArr[index] = val;
        }

        public void DisplayAll()
        {
            for (int i = 0; i < myArr.Length; i++)
            {
                Console.WriteLine(myArr[i]);
            }
        }
    }
}
