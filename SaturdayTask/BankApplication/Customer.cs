﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BankApplication
{
    class Customer
    {
        private static double RateOfInterest;
        private int accountNumber;
        private string name;
        private double balance;

        public Customer(int accountNumber, string name,double balance)
        {
            RateOfInterest = 100;
            this.name = name;
            this.accountNumber = accountNumber;
            this.balance = balance;
        }

        public void SetCustInfo(int accountNumber,string name,double balance)
        {
            this.name = name;
            this.accountNumber = accountNumber;
            this.balance = balance;
        }

        public virtual string GetCustInfo()
        {
            return "Customer Info : " + this.name + " " + this.accountNumber + " " + this.balance+" "+RateOfInterest;
        }

        public virtual double CalLoan()
        {
            return this.balance;
        }
    }
}
