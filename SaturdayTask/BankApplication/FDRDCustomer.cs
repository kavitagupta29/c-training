﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApplication
{
    class FDRDCustomer:Customer
    {
        private double fdrdAmount;

        public FDRDCustomer(int accountNumber, string name, double balance, double fdrdAmount)
            : base(accountNumber, name, balance)
        {
            this.fdrdAmount = fdrdAmount;
        }

        public override string GetCustInfo()
        {
            return base.GetCustInfo() + " " + this.fdrdAmount;
        }

        public override double CalLoan()
        {
            return this.fdrdAmount/5;
        }
    }
}
