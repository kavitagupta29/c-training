﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApplication
{
    class LoanCustomer:Customer
    {
        private double loan;

        public LoanCustomer(int accountNumber, string name, double balance, double loan)
            : base(accountNumber,name, balance)
        {
            this.loan = loan;
        }
        public override string GetCustInfo()
        {
            return base.GetCustInfo() + " " + this.loan;
        }

        public override double CalLoan()
        {
            return this.loan *2;
        }
    }
}
