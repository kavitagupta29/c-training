﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApplication
{
    class SavingAccount:Customer
    {
        private double savingBalance;

        public SavingAccount(int accountNumber, string name, double balance, double savingBalance)
            : base(accountNumber, name, balance)
        {
            this.savingBalance = savingBalance;
        }
        public override string GetCustInfo()
        {
            return base.GetCustInfo() + " " + this.savingBalance;
        }

        public override double CalLoan()
        {
            return this.savingBalance *3;
        }
    }
}
