﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer[] cust=new Customer[3];
            cust[0]=new FDRDCustomer(1111111,"Kavita",70000,50000);
            cust[1]=new LoanCustomer(2222222,"Chetan",30000,4000);
            cust[2]=new SavingAccount(333333,"Manisha",40000,30000);

            foreach (Customer c in cust)
            {
                Console.WriteLine(c.GetCustInfo());
                Console.WriteLine("Account Info is: " + c.CalLoan());
            }
        }
    }
}
