﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShapeLibrary;

namespace WinFormShapes
{
    public partial class Form1 : Form
    {
        private List<Shape> sList;
        public Form1()
        {
            
           
            InitializeComponent();
            DisableGroupBoxes();
            sList = new List<Shape>();
        }


        void DisableGroupBoxes()
        {
            groupBox1.Enabled = false;
            groupBox2.Enabled = false;
            groupBox3.Enabled = false;
            groupBox4.Enabled = false;


        }



        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (radioButton1.Checked==true)
                {
                    sList.Add(new Circle(textBox1.Text,Convert.ToDouble(textBox2.Text)));
                }
                else if (radioButton2.Checked==true)
                {
                    sList.Add(new ShapeLibrary.Rectangle(textBox1.Text, Convert.ToDouble(textBox3.Text),Convert.ToDouble(textBox4.Text)));
                }
                else if(radioButton3.Checked==true)
                {
                    sList.Add(new Tringle(textBox1.Text, Convert.ToDouble(textBox5.Text), Convert.ToDouble(textBox6.Text)));
                }
                else
                {
                    sList.Add(new Square(textBox1.Text,Convert.ToDouble(textBox7.Text)));
                }
            }
            catch (FormatException fEx)
            {
                MessageBox.Show("Please Enter valid input:!!!!!!!");
            }

            MessageBox.Show("Employee added into the list.....");
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            groupBox2.Enabled = true;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            groupBox3.Enabled = true;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            groupBox4.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Shapes.Items.Clear();
            foreach (Shape s in sList)
            {
                Shapes.Items.Add(s.GetShapeInfo() + "Area is = " + s.CalArea());
            }
        }

        private void Shapes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
