﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandling
{
    class Program
    {
        static void Main1(string[] args)
        {
            Console.WriteLine("App starts.....");
            int x = 1,y = 1;
            string val = "123";
            Class1 obj = new Class1();

            try
            {
                Class1.Func();
                obj.MyFunc();
                x = x/y;
                Console.WriteLine("value of x: " + x);
                y = int.Parse(val);
                Console.WriteLine("value of y: " + y);
                Console.WriteLine("Enter your name: ");
                val = Console.ReadLine();
                if (val.Contains("yogesh"))
                {
                    //throw new Exception("yogesh is not allowed!!!!!");
                    throw new CreateException("This is my own Exception");
                }

            }
            catch (DivideByZeroException divEx)
            {
                Console.WriteLine("Error 102: " + divEx.Message);
            }
            catch (FormatException fEx)
            {
                Console.WriteLine("Error 103: " + fEx.Message);
            }
            catch (NullReferenceException nullEx)
            {
                Console.WriteLine("Error 104: " + nullEx.Message);
            }
            catch (CreateException crExce)
            {
                Console.WriteLine("Error 105: " + crExce.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error 101: " + ex.Message);
            }

            finally
            {
                Console.WriteLine("finally called releasing resources.......");
            }
            Console.WriteLine("App Ends.....");


        }

        static void Main2(string[] args)
        {
            int y = 0, x = 1;
            Console.WriteLine("1");
            try
            {
                 x = x/y;
                Console.WriteLine(x);
            }
            finally
            {
                Console.WriteLine("3");
            }
            Console.WriteLine("4");
           
        }

        static void Main(string[] args)
        {
            int y = 0, x = 1;
            Console.WriteLine("1");
            try
            {
                x = x/y;
                Console.WriteLine(x);
            }
            catch (NullReferenceException nEx)
            {
                Console.WriteLine(nEx.Message);
            }
            catch (FormatException fEx)
            {
                Console.WriteLine(fEx.Message);
            }
            finally
            {
                Console.WriteLine("3");
            }
            Console.WriteLine("4");

        }
    }
}
