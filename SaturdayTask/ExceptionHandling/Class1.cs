﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandling
{
    class Class1
    {
        public static void Func()
        {

            Console.WriteLine("Inside static func of class 1");
        }

        public void MyFunc()
        {
            Console.WriteLine("Inside non static func of class 1");
        }
    }
}
