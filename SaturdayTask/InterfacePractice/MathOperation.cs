﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacePractice
{
    class MathOperation:IA,IB
    {
        public int Sub(int num1, int num2)
        {
            return num1 - num2;
        }

         int IA.Add(int num1, int num2)
        {
            Console.WriteLine("first Interface.....");
            return num1 + num2;
        }
        int IB.Add(int num1, int num2)
        {
            Console.WriteLine("Second Interface.....");
            return num1 + num2;
        }

        public int Add(int num1, int num2)
        {
            Console.WriteLine("Third Interface.....");
            return num1 + num2;
        }

    }
}
