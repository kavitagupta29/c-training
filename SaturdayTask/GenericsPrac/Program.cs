﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace GenericsPrac
{
    class Program
    {
        static void Main(string[] args)
        {
            bool result=Cal<string>.CheckValue("kavita", "Mona");
            if (result)
            {
                Console.WriteLine("Value are equal");
            }
            else
            {
                Console.WriteLine("Value are not equal");
            }
        }

    }

    public class Cal<T>
    {
        public static bool CheckValue(T value1, T value2)
        {
            return value1.Equals(value2);
        }
    }
}
