﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllShapes
{
    class Circle:Shape
    {
        private double radius;

        public Circle(string name, double radius) : base(name)
        {
            this.radius = radius;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() + " " + this.radius;
        }

        public override double CalArea()
        {
            return 3.14 * this.radius*this.radius;
        }
    }
}
