﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllShapes
{
    class Program
    {
        private static void Main(string[] args)
        {
            List<Shape> slist = new List<Shape>();
            slist.Add(new Circle("Circle", 4));
            slist.Add(new Rectangle("Rectangle", 12, 10));
            slist.Add(new Square("Square", 4));
            slist.Add(new Tringle("Tringle", 4, 6));
            slist.Add(new Square("Square1", 12));
            slist.Add(new Tringle("Tringle1", 5, 10));

            foreach (Shape  s in slist)
            {
                Console.WriteLine(s.GetShapeInfo()+ " Area="+s.CalArea());
            }
        }

        static void Main1(string[] args)
        {
            //Shape s = new Shape();

            Shape[] shapes=new Shape[4];
            shapes[0]=new Circle("Circle",4);
            shapes[1]=new Rectangle("Rectangle",12,10);
            shapes[2]=new Square("Square",4);
            shapes[3]=new Tringle("Tringle",4,6);

            foreach (Shape s in shapes)
            {
               Console.WriteLine(s.GetShapeInfo());
               Console.WriteLine("Area is: "+ s.CalArea()); 
            }
        }
    }
}
