﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("App starts.....");
            Thread t1 = new Thread(Func1);
            Thread t2 = new Thread(Func2);
            Thread t3 = new Thread(() => Func3("3333333333333......."));

            t1.IsBackground = true;
            t2.IsBackground = true;
           // t3.IsBackground = true;

            Console.WriteLine("Thread Created.....");
            t1.Start();
            t2.Start();
            t3.Start();

            
            Console.WriteLine("App Ends.....");
        }

        static void Func1()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("111111111111111......");
                Thread.Sleep(300);
            }
        }
        static void Func2()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("222222222222222......");
                Thread.Sleep(300);
            }
        }
        static void Func3(string data)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(data);
                Thread.Sleep(300);
            }
        }
    }
}
