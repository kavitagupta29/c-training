﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OfficeManagement;

namespace WindowsFormsOffice
{
    public partial class Form1 : Form
    {
        private List<Employee> eList;
        public Form1()
        {
            InitializeComponent();
            DisableGroupBoxes();
            eList=new List<Employee>();
        }

        void DisableGroupBoxes()
        {
            groupBox1.Enabled = false;
            groupBox2.Enabled = false;
            groupBox3.Enabled = false;



        }
      

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (comboBox1.SelectedIndex == 0)
            {
                DisableGroupBoxes();
                groupBox1.Enabled = true;
            }
            else if (comboBox1.SelectedIndex == 1)
            {
                DisableGroupBoxes();
                groupBox2.Enabled = true;
            }
            else
            {
                DisableGroupBoxes();
                groupBox3.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.SelectedIndex == 0)
                {
                    eList.Add(new Engineer(Convert.ToInt32(textBox1.Text), textBox2.Text,
                        Convert.ToDouble(textBox3.Text), Convert.ToDouble(textBox4.Text),
                        Convert.ToDouble(textBox5.Text)));
                }
                else if (comboBox1.SelectedIndex == 1)
                {
                    eList.Add(new Manager(Convert.ToInt32(textBox1.Text), textBox2.Text, Convert.ToDouble(textBox3.Text),
                        Convert.ToDouble(textBox4.Text), textBox5.Text));
                }
                else
                {
                    eList.Add(new SalesPerson(Convert.ToInt32(textBox1.Text), textBox2.Text,
                        Convert.ToDouble(textBox3.Text), Convert.ToDouble(textBox4.Text),
                        Convert.ToDouble(textBox5.Text)));
                }
            }
            catch (FormatException fEx)
            {
                MessageBox.Show("Please Enter valid input:!!!!!!!");
            }
           
            MessageBox.Show("Employee added into the list.....");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            foreach (Employee emp in eList)
            {
                listBox1.Items.Add(emp.GetEmpInfo() + "Total sal= " + emp.CalSal());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Employee Count :" + Employee.GetCount());
            MessageBox.Show("Employee Count :" + eList.Count);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox10.Text.Length < 3)
            {
                MessageBox.Show("No File Selected Yet !");
                return;
            }
            FileStream fs = null;
            StreamWriter sw = null;


            try
            {
                string fName = textBox10.Text;
                if (!File.Exists(fName))
                {
                    MessageBox.Show("File dowsn't exist");
                    return;
                }
                 fs = new FileStream(textBox10.Text, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
                 sw = new StreamWriter(fs);
                foreach (Employee emp in eList)
                {
                    sw.WriteLine(emp.GetEmpInfo() + "Total sal= " + emp.CalSal());
                }
                MessageBox.Show("File Write Successfully......");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
                if (fs != null)
                    fs.Close();

            }
            
        }

        private void button5_Click(object sender, EventArgs e)
        {

            if (textBox10.Text.Length < 3)
            {
                MessageBox.Show("No File Selected Yet !");
                return;
            }
            listBox1.Items.Clear();
            FileStream fs = null;
            StreamReader sr = null;


            try
            {
                fs = new FileStream(@"C:\temp\data.txt", FileMode.Open, FileAccess.Read, FileShare.Read);
                sr = new StreamReader(fs);

                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    listBox1.Items.Add(line);
                }
              

                MessageBox.Show("File Read Successfully......");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            
        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox10.Text = openFileDialog1.FileName;
            }
        }
    }
}
