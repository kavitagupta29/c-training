﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFileOperation
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            button3.Enabled = false;
            listBox2.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();

            folderDlg.ShowNewFolderButton = true;



            DialogResult result = folderDlg.ShowDialog();

            if (result == DialogResult.OK)

            {

                textBox1.Text = folderDlg.SelectedPath;

                Environment.SpecialFolder root = folderDlg.RootFolder;

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("No folder is selected");
                return;

            }
            string directory = textBox1.Text;
            List<string> textFiles = System.IO.Directory.GetFiles(directory, "*.txt").ToList();
            foreach (string s in textFiles)
            {
                listBox1.Items.Add(s);
            }
            MessageBox.Show("Files are dispalyed");
            button3.Enabled = true;
            listBox2.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (listBox1.SelectedItem == null)
                {
                    MessageBox.Show("No file is selected");
                    return;
                }

                string s = listBox1.SelectedItem.ToString();


                FileStream fs = new FileStream(s, FileMode.Open);
                StreamReader sr = new StreamReader(fs);

                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    listBox2.Items.Add(line);
                }


                MessageBox.Show("File Read Successfully......");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }



        }
    }
}

