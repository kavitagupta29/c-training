﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    class Engineer:Employee
    {
        private double extraHrs;
        private double income;

        public Engineer(int id, string name, double sal, double extraHrs, double income):base(id,name,sal)
        {
            this.extraHrs = extraHrs;
            this.income = income;
        }
        //public new string GetEmpInfo()  -----for Function hiding(new)
        public override string GetEmpInfo()
        {
            return base.GetEmpInfo() + " " + this.extraHrs + " " + this.income +"---Eng";
        }
        //public new double CalSal()   -----for Function hiding(new)
        public override double CalSal()
        {
            return this.salary + (this.income*this.extraHrs) ;
        }
    }
}
