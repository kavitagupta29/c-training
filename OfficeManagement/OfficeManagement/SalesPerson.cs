﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    class SalesPerson:Employee
    {
        private double sales;
        private double com;

        public SalesPerson(int id, string name, double salary, double sales, double com) : base(id, name, salary)
        {
            this.sales = sales;
            this.com = com;
        }

       // public new string GetEmpInfo() -----for Function hiding(new)
        public override string GetEmpInfo()
        {
            return base.GetEmpInfo() + " " + this.sales + " " + this.com +"---Sp";
        }

        // public new double CalSal()  -----for Function hiding(new)
        public override double CalSal()
        {
            return this.salary +(this.sales * this.com);
        }
    }
}
