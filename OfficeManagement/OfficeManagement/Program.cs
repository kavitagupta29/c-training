﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    class Program
    {

        // Employee class

        static void Main1(string[] args)
        {
            Console.WriteLine("App is started...");

            Console.WriteLine("Count of employess is:" + Employee.GetCount());

            // Employee.SetCompany("Tricentis"); 

            // Default Constructor
            Employee e1 = new Employee();
            /* e.name = "xyz";
             Console.WriteLine(e.name);*/
            /* e1.SetEmpInfo(1,"Kavita",100000);*/
            Console.WriteLine(e1.GetEmpInfo());


            // Default Constructor
            Employee e2 = new Employee();
            /*e2.SetEmpInfo(2, "Manisha", 100000);*/
            Console.WriteLine(e2.GetEmpInfo());


            // Parameterized Constructor
            Employee e3 = new Employee(3, "Harshad", 30000);
            Console.WriteLine(e3.GetEmpInfo());

            Employee e5 = new Employee("Chetan", 5, 70000);
            Console.WriteLine(e5.GetEmpInfo());



            // Copy Constructor
            Employee e4 = new Employee(e3);
            Console.WriteLine(e4.GetEmpInfo());
            Console.WriteLine("Count of employess is:" + Employee.GetCount());



            Console.ReadLine();



        }

        //Util class

        static void Main2(string[] args)
        {
            MyUtilClass util = new MyUtilClass();
            int[] array = { 1, 2, 3, 4, 5 };

            Console.WriteLine("Addition of two Number is:" + util.Add(2, 2));
            Console.WriteLine("Addition of three Number is:" + util.Add(2, 2, 2));
            Console.WriteLine("Concatenation of two string is:" + util.Add("Kavita", "Gupta"));
            Console.WriteLine("Addition of array is:" + util.Add(array));
            Console.ReadLine();


        }
        // Emplist class

        static void Main3(string[] args)
        {


            EmpList.AddEmployees();
            Employee[] myEmps = EmpList.GetEmployee();
            //Console.WriteLine(myEmps[0].GetEmpInfo());

            /*for (int i = 0; i < myEmps.Length; i++)
            {
                Console.WriteLine(myEmps[i].GetEmpInfo());
            }*/

            foreach (Employee e in myEmps)
            {
                Console.WriteLine(e.GetEmpInfo());
            }
            Console.ReadLine();
        }

        static void Main5(string[] args)
        {
            Employee em1 = new SalesPerson(1, "Kavita", 20000, 5690, 20050);


            Employee em2 = new Engineer(2, "Pranita", 3000, 6, 9000);


            Employee em3 = new Manager(3, "Harshad", 4000, 3000, "HR");


            Console.WriteLine(em1.GetEmpInfo());


            Console.WriteLine(em2.GetEmpInfo());


            Console.WriteLine(em3.GetEmpInfo());

        }

        static void Main4(string[] args)
        {

            SalesPerson s1 = new SalesPerson(1, "Kavita", 20000, 5690, 20050);


            Engineer e1 = new Engineer(2, "Pranita", 3000, 6, 9000);


            Manager m1 = new Manager(3, "Harshad", 4000, 3000, "HR");


            Console.WriteLine(s1.GetEmpInfo());
            Console.WriteLine("Total sal:" + s1.CalSal());


            Console.WriteLine(e1.GetEmpInfo());
            Console.WriteLine("Total sal:" + e1.CalSal());


            Console.WriteLine(m1.GetEmpInfo());
            Console.WriteLine("Total sal:" + m1.CalSal());

        }


        static void Main(string[] args)
        {
            Employee[] err = new Employee[3];
            err[0] = new SalesPerson(1, "Kavita", 20000, 5690, 20050);
            err[1] = new Engineer(2, "Pranita", 3000, 6, 9000);
            err[2] = new Manager(3, "Harshad", 4000, 3000, "HR");

            foreach (Employee e in err)
            {
                Console.WriteLine(e.GetEmpInfo());
                Console.WriteLine("Total Sal: " + e.CalSal());
            }
        }

    }



}
