﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    class MyUtilClass
    {
        public int Add(int n1, int n2)
        {
            return n1 + n2;
        }
        public int Add(int n1, int n2,int n3)
        {
            return n1 + n2+ n3 ;
        }

        public string Add(string str1, string str2)
        {
            return str1 +" "+ str2;
        }

        public int Add(int[] ar)
        {
          return ar.Sum();
        }

    }
}
