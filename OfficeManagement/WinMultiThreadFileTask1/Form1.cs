﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinMultiThreadFileTask1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length < 3)
            {
                MessageBox.Show("No File Selected Yet !");
                return;
            }
            listBox1.Items.Clear();
            FileStream fs = null;
            StreamReader sr = null;


            try
            {
                fs = new FileStream(@"C:\temp\data.txt", FileMode.Open, FileAccess.Read, FileShare.Read);
                sr = new StreamReader(fs);

                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    listBox1.Items.Add(line);
                }


                MessageBox.Show("File Read Successfully......");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
           /* if (textBox1.Text.Length < 3)
            {
                MessageBox.Show("No File Selected Yet !");
                return;
            }*/
            FileStream fs = null;
            StreamWriter sw = null;


            try
            {
                string fName = textBox2.Text;
                if (!File.Exists(fName))
                {
                    MessageBox.Show("File dowsn't exist");
                    return;
                }
                fs = new FileStream(/*@"C:\temp\Text.txt"*/textBox2.Text, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
                sw = new StreamWriter(fs);
                /*string txt = listBox1.Text;
                sw.WriteLine(txt);*/
                for (int i = 0; i < 500; i++)
                {
                    sw.WriteLine("Tosca is a software testing tool.....");
                }
                MessageBox.Show("File Write Successfully......");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
                if (fs != null)
                    fs.Close();

            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = saveFileDialog1.FileName;
            }
        }

        public void ReadFile()
        {
            if (textBox1.Text.Length < 3)
            {
                MessageBox.Show("No File Selected Yet !");
                return;
            }
            listBox1.Items.Clear();
            FileStream fs = null;
            StreamReader sr = null;


            try
            {
                fs = new FileStream(@"C:\temp\data.txt", FileMode.Open, FileAccess.Read, FileShare.Read);
                sr = new StreamReader(fs);
                
                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    //Thread.Sleep(100);
                    listBox1.Items.Add(line);
                  //line=  line.Replace('a', 'A');
                    
                   // listBox2.Items.Add(line);
                    //Thread.Sleep(100);
                }


                MessageBox.Show("File Read Successfully......");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
        }

        public void WriteFile()
        {
            FileStream fs = null;
            StreamWriter sw = null;

            
            try
            {
                string fName = textBox2.Text;
                if (!File.Exists(fName))
                {
                    MessageBox.Show("File doesn't exist");
                    return;
                }
                fs = new FileStream(/*@"C:\temp\Text.txt"*/textBox2.Text, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
                sw = new StreamWriter(fs);
            
                for (int i = 0; i < 100; i++)
                {
                    sw.WriteLine("Tosca is a software testing tool.....");
                    //Thread.Sleep(300);
                }
                MessageBox.Show("File Write Successfully......");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
                if (fs != null)
                    fs.Close();

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Thread t1=new Thread(ReadFile);
           
            Thread t2=new Thread(WriteFile);

            t1.Start();
            t2.Start();
        }

     

        public void Task2()
        {
            try
            {
                Thread.Sleep(200);
                foreach (string s in listBox1.Items)
                {
                   Thread.Sleep(300);
                   string t = s.Replace('a', 'A');
                   listBox2.Items.Add(t);
                }

               
                MessageBox.Show("File Write Successfully......");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Thread t3 = new Thread(ReadFile);
            
            Thread t4 = new Thread(Task2);
            t3.Start();
           
            t4.Start();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
