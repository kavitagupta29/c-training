﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinAdoNetForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                conn =
                    new SqlConnection(
                        @"Data Source=TRI-NB0553\SQLEXPRESS;Initial Catalog=Training;User ID=tosca;Password=tosca");
                SqlCommand cmd = new SqlCommand("select * from employee", conn);
                conn.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    MessageBox.Show("Emp info: " + sdr[0] + sdr["Name"] + sdr.GetDecimal(2) + sdr.GetString(3));
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                conn.Close();
            }
           
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                conn =
                    new SqlConnection(
                        @"Data Source=TRI-NB0553\SQLEXPRESS;Initial Catalog=Training;User ID=tosca;Password=tosca");
                SqlCommand cmd = new SqlCommand("select * from employee", conn);
                conn.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
               
                    /*DataTable dt = new DataTable();

                    dt.Load(sdr);

                    dataGridView1.DataSource = dt;*/

                BindingSource bs=new BindingSource();
                bs.DataSource = sdr;
                dataGridView1.DataSource = bs;
                conn.Close();

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                conn.Close();
            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                conn =
                    new SqlConnection(
                        @"Data Source=TRI-NB0553\SQLEXPRESS;Initial Catalog=Training;User ID=tosca;Password=tosca");
                SqlCommand cmd = new SqlCommand("select count(*) from employee", conn);
                conn.Open();
                int count=Convert.ToInt32(cmd.ExecuteScalar());
                MessageBox.Show("Count : " + count);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
